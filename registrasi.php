<?php
session_start();
include_once 'po-library/po-database.php';
include_once 'po-library/po-function.php';
$val = new Povalidasi;
if (!$_SESSION['submit']){
	header("location:404.php");
}else{
	if(empty($_POST['reg_name']) || empty($_POST['reg_email']) || empty($_POST['reg_identity']) || empty($_POST['reg_address'])){
		echo "<script language='javascript'>
            window.alert('Data belum lengkap');
			window.location.href='/registrasi';
        </script>";
	}else{
		$reg_name = $val->validasi($_POST['reg_name'],'xss');
		$reg_email = $val->validasi($_POST['reg_email'],'xss');
		$reg_identity = $val->validasi($_POST['reg_identity'],'xss');
		$reg_address = $val->validasi($_POST['reg_address'],'xss');
                $reg_phone = $val->validasi($_POST['reg_phone'],'xss');
                $reg_unit = $val->validasi($_POST['reg_unit'],'xss');
		$message = "<html>
			<body>
				Nama : $reg_name<br />
				Email : $reg_email<br />
				Alamat : $reg_address<br /><br />
				Send Date : $hari_ini, $tgl_skrg-$bln_sekarang-$thn_sekarang ($jam_sekarang WIB)
			</body>
			</html>";
		$table = new PoTable('registrasi');
		$table->save(array(
			'reg_name' => $reg_name,
			'reg_email' => $reg_email,
			'reg_phone' => $reg_phone,
                        'reg_unit' => $reg_unit,
                        'reg_identity' => $reg_identity,
			'reg_address' => $reg_address
		));
		unset($_POST);
		echo "<script language='javascript'>
            window.alert('Succesfully Send Message');
            window.location.href='/registrasi';
        </script>";
	}
}
?>
