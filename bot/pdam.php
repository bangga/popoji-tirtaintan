<?php

define('BOT_TOKEN', '197852845:AAFD5k1GXO_m8d-bENDHsguRTczE_S8mZx0');
define('API_URL', 'https://api.telegram.org/bot'.BOT_TOKEN.'/');

function apiRequestWebhook($method, $parameters) {
  if (!is_string($method)) {
    error_log("Method name must be a string\n");
    return false;
  }

  if (!$parameters) {
    $parameters = array();
  } else if (!is_array($parameters)) {
    error_log("Parameters must be an array\n");
    return false;
  }

  $parameters["method"] = $method;

  header("Content-Type: application/json");
  echo json_encode($parameters);
  return true;
}

function exec_curl_request($handle) {
  $response = curl_exec($handle);

  if ($response === false) {
    $errno = curl_errno($handle);
    $error = curl_error($handle);
    error_log("Curl returned error $errno: $error\n");
    curl_close($handle);
    return false;
  }

  $http_code = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));
  curl_close($handle);

  if ($http_code >= 500) {
    // do not wat to DDOS server if something goes wrong
    sleep(10);
    return false;
  } else if ($http_code != 200) {
    $response = json_decode($response, true);
    error_log("Request has failed with error {$response['error_code']}: {$response['description']}\n");
    if ($http_code == 401) {
      throw new Exception('Invalid access token provided');
    }
    return false;
  } else {
    $response = json_decode($response, true);
    if (isset($response['description'])) {
      error_log("Request was successfull: {$response['description']}\n");
    }
    $response = $response['result'];
  }

  return $response;
}

function apiRequest($method, $parameters) {
  if (!is_string($method)) {
    error_log("Method name must be a string\n");
    return false;
  }

  if (!$parameters) {
    $parameters = array();
  } else if (!is_array($parameters)) {
    error_log("Parameters must be an array\n");
    return false;
  }

  foreach ($parameters as $key => &$val) {
    // encoding to JSON array parameters, for example reply_markup
    if (!is_numeric($val) && !is_string($val)) {
      $val = json_encode($val);
    }
  }
  $url = API_URL.$method.'?'.http_build_query($parameters);

  $handle = curl_init($url);
  curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
  curl_setopt($handle, CURLOPT_TIMEOUT, 60);

  return exec_curl_request($handle);
}

function apiRequestJson($method, $parameters) {
  if (!is_string($method)) {
    error_log("Method name must be a string\n");
    return false;
  }

  if (!$parameters) {
    $parameters = array();
  } else if (!is_array($parameters)) {
    error_log("Parameters must be an array\n");
    return false;
  }

  $parameters["method"] = $method;

  $handle = curl_init(API_URL);
  curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
  curl_setopt($handle, CURLOPT_TIMEOUT, 60);
  curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parameters));
  curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

  return exec_curl_request($handle);
}

function processMessage($message) {
  // process incoming message
  $message_id = $message['message_id'];
  $chat_id = $message['chat']['id'];
  $first_name = $message['chat']['first_name'];
  if (isset($message['text'])) {
    // incoming text message
    $text = strtolower($message['text']);

    if (strpos($text, "/start") === 0) {
      apiRequestJson("sendMessage", array('chat_id' => $chat_id, "text" => 'Hallo '.$first_name.'.. Selamat datang di info tagihan PDAM Tirta Intan silahkan masukkan nomor SL anda', 'reply_markup' => array(
        //'keyboard' => array(array('Hello', 'Hi')),
        'one_time_keyboard' => false,
        'resize_keyboard' => true)));
    } else if ($text === "cek air" || $text === "cekair" || $text === "cek tagihan" || $text === "tagihan") {
      apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => "Silahkan masukkan Nomor SL anda.."));
    } else if (strlen($text) == 6) {


	// create a new cURL resource
	$ch = curl_init();

	$_SERVER['HTTP_X_REAL_IP'] = $_SERVER['REMOTE_ADDR'];
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, 'https://secang.simeut.my.id:8080/tirtaintan-publik-data/enquiry/telegram/'.$text);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_SERVER));
	//curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '.$_SERVER['REMOTE_ADDR']));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$kembalian   = (array) json_decode(curl_exec($ch));

	$edit_respon = $kembalian['error'].PHP_EOL;
	$tagihan = 0;
	for($i=0;$i<count($kembalian['data']);$i++){
		$rekening = (array) $kembalian['data'][$i];
		if($i==0){
			$edit_respon .= "No SL : ".$rekening['pel_no'].PHP_EOL;
			$edit_respon .= "Nama : ".$rekening['pel_nama'].PHP_EOL;
			$edit_respon .=  "Alamat : ".$rekening['pel_alamat'].PHP_EOL;
		}
		$tagihan = $tagihan + $rekening['rek_total'];
	}
	if ($i>0){
		$edit_respon .= "Total tagihan : Rp. ".number_format($tagihan, 2, ',', '.').PHP_EOL;
	}

	$edit_respon .= "Referensi : ".$kembalian['token'].PHP_EOL;
	$edit_respon .= "Selengkapnya di https://pdam.tirtaintan.co.id/info-tagihan/".$text.PHP_EOL;


    
      apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => $edit_respon));
    } else if (strpos($text, "/stop") === 0) {
      // stop now 
    } else {
      apiRequestWebhook("sendMessage", array('chat_id' => $chat_id, "reply_to_message_id" => $message_id, "text" => 'Nomor SL yang anda masukkan salah, silahkan ulangi.'));
    }
  } else {
    apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => 'Masukan 6 digit nomer sambungan'));
  }
}


define('WEBHOOK_URL', 'https://pdam.tirtaintan.co.id/bot/');

if (php_sapi_name() == 'cli') {
  // if run from console, set or delete webhook
  apiRequest('setWebhook', array('url' => isset($argv[1]) && $argv[1] == 'delete' ? '' : WEBHOOK_URL));
  exit;
}


$content = file_get_contents("php://input");
$update = json_decode($content, true);

if (!$update) {
  // receive wrong update, must not happen
  exit;
}

if (isset($update["message"])) {
  processMessage($update["message"]);
}
