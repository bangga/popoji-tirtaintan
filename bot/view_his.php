<?php
    header('Content-Type: application/json');
    header('Cache-Control: no-cache');
    header('Access-Control-Allow-Origin: https://pdam.tirtaintan.co.id');

    $_POST = array("start"=>0, "length"=>1, "periode"=>$_GET['periode']);

    // create a new cURL resource
    $ch = curl_init();

    // set URL and other appropriate options
    curl_setopt($ch, CURLOPT_URL, 'https://secang.simeut.my.id:8080/tirtaintan-replika-publik/his/website/'.$_GET['data']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_SERVER + $_POST));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '.$_SERVER['REMOTE_ADDR']));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $json_output = json_decode(curl_exec($ch));
	
    // close cURL resource, and free up system resources
    curl_close($ch);

    $errno = 3;
    $error = "Data tidak ditemukan";
    $data_dil = array();

    if(count($json_output->data)>0){
        $errno = 0;
        $error = "";
        $data_his = $json_output->data;
    }

    $array_output['token'] = $json_output->token;
    $array_output['errno'] = $errno;
    $array_output['error'] = $error;
    $array_output['data'] = $data_his;

    echo json_encode($array_output).PHP_EOL;

    flush();

