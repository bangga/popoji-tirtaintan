<?php
    header('Content-Type: application/json');
    header('Cache-Control: no-cache');
    if($_SERVER['SERVER_NAME']=='perumda.tirtaintan.co.id') header('Access-Control-Allow-Origin: https://perumda.tirtaintan.co.id');
    else header('Access-Control-Allow-Origin: https://pdam.tirtaintan.co.id');

    // create a new cURL resource
    $ch = curl_init();

    // set URL and other appropriate options
    curl_setopt($ch, CURLOPT_URL, 'https://secang.simeut.my.id:8080/tirtaintan-publik-data/publik-data/info-tagihan/website/'.$_GET['data']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_SERVER + $_POST));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '.$_SERVER['REMOTE_ADDR']));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    echo curl_exec($ch);
	
    // close cURL resource, and free up system resources
    curl_close($ch);

    flush();

