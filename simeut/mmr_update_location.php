<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: https://simeut.tirtaintan.co.id');

    // get agent parameter
    $remote_addr = $_SERVER['REMOTE_ADDR'];
    $user_agent  = $_SERVER['HTTP_USER_AGENT'];
    $param       = array('remote_addr'=>$remote_addr, 'user_agent'=>$user_agent);

	// create a new cURL resource
	$ch = curl_init();

	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, 'http://api.tirtaintan.co.id/mmr_update_location.php');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST+$param));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	echo curl_exec($ch);
	
	// close cURL resource, and free up system resources
	curl_close($ch);


    flush();
?>
