<?php
   header('Content-Type: application/json');
   header('Cache-Control: no-cache');
   header('Access-Control-Allow-Origin: pdam.tirtaintan.co.id');

   $targetUrl  = "https://secang.simeut.my.id/core-data/login/create";
   $targetPort = "8080";
   $data_array = array("data" => array(array("name" => "kar_id", "value" => $_POST['kar_id']), array("name" => "kar_pass", "value" => $_POST['kar_pass'])));

   // create a new cURL resource
   $ch = curl_init();

   // set URL and other appropriate options
   curl_setopt($ch, CURLOPT_URL, $targetUrl);
   curl_setopt($ch, CURLOPT_PORT, $targetPort);
   curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data_array));
   curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '.$_SERVER['REMOTE_ADDR']));
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   $data_raw = json_decode(curl_exec($ch));

   // close cURL resource, and free up system resources
   curl_close($ch);

   echo json_encode($data_raw->data).PHP_EOL;

   flush();

