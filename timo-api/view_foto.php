<?php
    header('Content-Type: image/jpeg');

    $pel_no = str_replace('m','',$_GET['nosl']);
    if(substr($pel_no, -3) == 'jpg'){
        $filename = $_SERVER['DOCUMENT_ROOT']."/".$_GET['bulan']."/".$pel_no;
    }
    else{
        $filename = $_SERVER['DOCUMENT_ROOT']."/".$_GET['bulan']."/".$pel_no.".jpg";
    }
    if(!is_readable($filename)){
	// create a new cURL resource
	$ch = curl_init();

	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, "https://secang.simeut.my.id:8080/tirtaintan-foto-cater/".$_GET['bulan']."/".$_GET['nosl']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	echo curl_exec($ch);

	// close cURL resource, and free up system resources
	curl_close($ch);
    }
    else{
        list($width, $height) = getimagesize($filename);

        // File and new size
        if(substr($_GET["nosl"],-1) == "m"){
            $newwidth = 480;
        }
        else{
            $newwidth = $width;
        }
        $newheight = $height * $newwidth / $width;

        // Load
        $thumb = imagecreatetruecolor($newwidth, $newheight);
        $source = imagecreatefromjpeg($filename);

        // Resize
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        // Output
        imagejpeg($thumb);
    }

