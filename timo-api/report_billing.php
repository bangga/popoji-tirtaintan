<?php
   if(isset($_GET['json'])){
      header('Content-Type: application/json');
   }
   else{
      header('Content-Type: text/event-stream');
   }
   header('Cache-Control: no-cache');
   header('Access-Control-Allow-Origin: https://dashboard.tirtaintan.co.id');

   // create a new cURL resource
   $ch = curl_init();

   $data_array = array();
   if(isset($_GET['json'])){
      $data_array['content_type'] = "json";
   }

   if(isset($_GET['table'])){
      $data_array['table_name'] = "db_".str_replace('-','_',$_GET['table']);
   }

   if(isset($_POST['filter'])){
      $data_array['filter'] = $_POST['filter'];
   }

   // set URL and other appropriate options
   curl_setopt($ch, CURLOPT_URL, "https://secang.simeut.my.id:8080/tirtaintan-report-billing/".str_replace('-','_',$_GET['param']).".php");
   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_array));
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   echo curl_exec($ch);

   // close cURL resource, and free up system resources
   curl_close($ch);

   flush();

