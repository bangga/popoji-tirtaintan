<?php
   header('Content-Type: application/json');
   header('Cache-Control: no-cache');
   header('Access-Control-Allow-Origin: pdam.tirtaintan.co.id');

   function put_log($raw_data){
      $fp = fopen('data_raw.log','a');
      fwrite($fp, $raw_data.PHP_EOL);
      fclose($fp);
   }

   // create a new cURL resource
   $ch = curl_init();

   // set URL and other appropriate options
   $requestMessage = json_decode(file_get_contents('php://input'));
   $dataRegister = (array) $requestMessage->data; 
   $dataRegister['reg_id'] = '8';
   $data_post = $dataRegister + $_SERVER;
   // put_log(json_encode($data_post));

   $targetUrl = "https://secang.simeut.my.id:8080/tirtaintan-replika-publik/timo/registrasi";
   $targetUrl = "https://mahoni.simeut.my.id/62ec5e9b494f0/put-registrasi/timo";
   curl_setopt($ch, CURLOPT_URL, $targetUrl);
   curl_setopt($ch, CURLOPT_POST, 1);
   curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data_post));
   curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '.$_SERVER['REMOTE_ADDR']));
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   // {"token":"62ecde6bcff16","errno":0,"error":"Data register: 814768 telah disimpan","reg_id":"814768","data":{"pem_reg":"814768"}}
   $data_obj = json_decode(curl_exec($ch));
   if(!isset($data_obj->reg_id)){
      if(isset($data_obj->data->pem_reg)){
         $data_obj->reg_id = $data_obj->data->pem_reg;
      }
      else{
         $data_obj->errno = 1;
         $data_obj->error = "terjadi gangguan teknis";
      }
   }

   if(isset($data_obj->data)){
      unset($data_obj->data);
   }

   $data_json = json_encode($data_obj);
   put_log($data_json);
   echo $data_json.PHP_EOL;
	
   // close cURL resource, and free up system resources
   curl_close($ch);

   flush();

