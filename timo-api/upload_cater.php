<?php
    header('Content-Type: application/json');
    header('Cache-Control: no-cache');
    header('Access-Control-Allow-Origin: https://pdam.tirtaintan.co.id');

    $requestMessage = file_get_contents('php://input');
    $data_array = (array) json_decode($requestMessage);

    function put_log($raw_data){
        $fp = fopen('data_raw.log','a');
	fwrite($fp, $raw_data.PHP_EOL);
	fclose($fp);
    }

    $data_array = $_POST;
    $data_array['HTTP_X_REAL_IP'] = $_SERVER['REMOTE_ADDR'];
    if(isset($_POST['nama_photo'])){
        unset($data_array['photo']);
    }

    // create a new cURL resource
    $ch = curl_init();

    $targetUrl  = "https://secang.simeut.my.id:8080";

    // set URL and other appropriate options
    curl_setopt($ch, CURLOPT_URL, $targetUrl."/tirtaintan-cater-offline-upload?".$_GET['data']);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_array));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '.$_SERVER['REMOTE_ADDR']));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $data_respon = (array) json_decode(curl_exec($ch));

    // close cURL resource, and free up system resources
    curl_close($ch);

    if(isset($data_respon['wdsml_pel_no']) && isset($_POST['nama_photo'])){
        $periode = $data_respon['wdsml_thn_baca'].$data_respon['wdsml_bln_baca'];
        $folder_to = $_SERVER['DOCUMENT_ROOT'].'/'.$periode;
        if(!is_dir($folder_to)){
            mkdir($folder_to, 0755, true);
        }
        $fp = fopen($folder_to.'/'.$data_respon['wdsml_pel_no'].'.jpg','w');
        fwrite($fp, base64_decode($_POST['photo']));
        fclose($fp);
    }

    if(isset($_POST['photo'])){
       unset($_POST['photo']);
    }
    put_log(json_encode($_POST + $data_respon));

    $errno = 1;
    if(isset($data_respon['errno'])){
       if($data_respon['errno'] == 0){
           $errno = 0;
       }
    }

    if($errno == 0){
       echo "OK";
    }
    else{
       echo "NOK";
    }

    flush();

