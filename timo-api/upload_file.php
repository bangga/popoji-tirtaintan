<?php
    header('Content-Type: application/json');
    header('Cache-Control: no-cache');
    header('Access-Control-Allow-Origin: https://pdam.tirtaintan.co.id');

    $token = uniqid();
    $errno = 4;
    $error = "File tidak diupload";

    $data_array                   = $_POST;
    $data_array['HTTP_X_REAL_IP'] = $_SERVER['REMOTE_ADDR'];
    $data_array['file_gambar']    = 0;
    if(isset($_FILES['file_gambar']['tmp_name'])){
        if($_FILES['file_gambar']['error']==0){
            $errno = 0;
            $data_array['file_gambar'] = 1;
        }
    }

    // create a new cURL resource
    $ch = curl_init();

    $targetUrl  = "https://secang.simeut.my.id";
    $targetPort = "8080";

    // set URL and other appropriate options
    curl_setopt($ch, CURLOPT_URL, $targetUrl."/tirtaintan-replika-publik/timo/upload/".$_GET['data']);
    curl_setopt($ch, CURLOPT_PORT, $targetPort);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_array));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '.$_SERVER['REMOTE_ADDR']));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $data_respon = (array) json_decode(curl_exec($ch));
    echo json_encode($data_respon).PHP_EOL; die();
    $token       = $data_respon['token'];
    $errno       = $data_respon['errno'];
    $error       = $data_respon['error'];

    // close cURL resource, and free up system resources
    curl_close($ch);

    if($errno == 0 && $data_post['file_gambar'] == 1){
        $file_output = "/home/pdamtir2/www/timo-api/".$token.".jpg";
        move_uploaded_file($_FILES['file_gambar']['tmp_name'], $file_output);
        $error = "Upload berhasil";
    }
    else if($errno == 0){
        $error = "Upload data berhasil";
    }
    else{
        $errno = 3;
    }

    $data_out['token'] = $token;
    $data_out['errno'] = $errno;
    $data_out['error'] = $error;
    $data_out['data']  = $_POST;
    echo json_encode($data_out).PHP_EOL;

    flush();

