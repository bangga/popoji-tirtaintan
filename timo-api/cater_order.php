<?php
   header('Content-Type: application/json');
   header('Cache-Control: no-cache');
   header('Access-Control-Allow-Origin: pdam.tirtaintan.co.id');

   $targetUrl  = "https://secang.simeut.my.id";
   $targetPort = "8080";
   $data_array = array("table_name" => "wmmr-dsml", "content_type" => "json");
   if($_POST['kar_id'] == "timo") $data_array['filter'][] = array("name" => "wdsml_pel_no", "value" => $_GET['data']);
   else $data_array['filter'][] = array("name" => "kar_id", "value" => $_POST['kar_id']);

   // create a new cURL resource
   $ch = curl_init();

   // set URL and other appropriate options
   curl_setopt($ch, CURLOPT_URL, $targetUrl."/tirtaintan-report-billing/referensi.php");
   curl_setopt($ch, CURLOPT_PORT, $targetPort);
   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_array));
   curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '.$_SERVER['REMOTE_ADDR']));
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   echo curl_exec($ch);
	
   // close cURL resource, and free up system resources
   curl_close($ch);

   flush();

