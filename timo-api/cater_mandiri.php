<?php
    header("Content-Type: application/json");
    header('Cache-Control: no-cache');
    header('Access-Control-Allow-Origin: pdam.tirtaintan.co.id');

    function put_log($raw_data){
        $fp = fopen('data_mandiri.log','a');
	fwrite($fp, $raw_data.PHP_EOL);
	fclose($fp);
    }

    $url = "https://timo.wipay.id/api/mandiri/".$argv[1];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $res = json_decode(curl_exec($ch));
    curl_close($ch);

    if(count($res->data) == 0){
        $raw_log = date('Y-m-d H:i:s')." ".$argv[1]." bacaan mandiri tidak ditemukan";
        put_log($raw_log);
        echo $raw_log.PHP_EOL;
    }

    foreach($res->data as $value){
        $raw_log = date('Y-m-d H:i:s')." ".$argv[1]." ".$value->no_sl;

        $url = "https://rasamala.tirtaintan.co.id/timo/order-cater/".$value->no_sl;
        $data_array['kar_id'] = "timo";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data_array));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = json_decode(curl_exec($ch));
        curl_close($ch);

        $wdsml_data = array();
        if(isset($res[0])) $wdsml_data = $res[0];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $value->photo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        $rescode = curl_getinfo($ch);
        curl_close($ch);

        if($rescode['size_download']<1000){
            $raw_log .= " foto tidak dapat didownload";
        }
        else if(count($wdsml_data) == 0){
            $raw_log .= " meter sudah dibaca";
        }
        else{
            $data_array['wmmr_id'] = $wdsml_data->wrute_id;
            $data_array['wmmr_standbaca'] = $value->angka_meter;
            $data_array['wmmr_abnormwm'] = 0;
            $data_array['wmmr_abnormenv'] = 0;
            $data_array['wmmr_note'] = "Mandiri";
            $data_array['lonkor'] = "107.8895491";
            $data_array['latkor'] = "-7.2404927";
            $data_array['HTTP_X_REAL_IP'] = "103.163.138.27";

            // create a new cURL resource
            $ch = curl_init();

            $targetUrl  = "https://secang.simeut.my.id:8080";

            // set URL and other appropriate options
            curl_setopt($ch, CURLOPT_URL, $targetUrl."/tirtaintan-cater-offline-upload?".$_GET['data']);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_array));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Real-IP: 103.163.138.27"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $data_respon = (array) json_decode(curl_exec($ch));

            // close cURL resource, and free up system resources
            curl_close($ch);

            if(isset($data_respon['errno'])){
                if($data_respon['errno'] == 0){
                    $raw_log .= " foto sudah disimpan";

                    $periode = $wdsml_data->wdsml_thn_baca.$wdsml_data->wdsml_bln_baca;
                    $folder_to = "/home/pdamtir2/public_html/".$periode;
                    if(!is_dir($folder_to)){
                        mkdir($folder_to, 0755, true);
                    }
                    $fp = fopen($folder_to."/".$wdsml_data->wdsml_pel_no.".jpg","w");

                    fwrite($fp, $res);
                    fclose($fp);
                }
                else{
                    $raw_log .= " data tidak dapat disimpan";
                }
            }
            else{
                $raw_log .= " data tidak dapat dikirim";
            }
        }

        put_log($raw_log);
        echo $raw_log.PHP_EOL;
    }

