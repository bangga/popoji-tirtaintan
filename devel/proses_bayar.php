<?php
   header('Content-Type: application/json');
   header('Cache-Control: no-cache');
   header('Access-Control-Allow-Origin: pdam.tirtaintan.co.id');

   $requestMessage = (array) json_decode(file_get_contents('php://input'));

   include $_SERVER['DOCUMENT_ROOT']."/devel/logging.php";
   $log    = new errorLog();

   define("_KODE", "devel");
   define("_USER", "loketwpy");
   define("_HOST", $_SERVER['REMOTE_ADDR']);

   // create a new cURL resource
   $ch = curl_init();

   // set URL and other appropriate options
   curl_setopt($ch, CURLOPT_URL, 'https://secang.simeut.my.id:8080/tirtaintan-replika-publik/payment/'._USER.'/'.$_GET['data']);
   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_SERVER+$requestMessage));
   curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '._HOST));
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   $raw_respon  = curl_exec($ch);

   $json_respon = json_decode($raw_respon);
   define("_TOKN", $json_respon->token);
   $log->logMess(bin2hex($raw_respon));

   echo $raw_respon;
	
   // close cURL resource, and free up system resources
   curl_close($ch);

   flush();

