<?php
   header('Content-Type: application/json');
   header('Cache-Control: no-cache');
   header('Access-Control-Allow-Origin: pdam.tirtaintan.co.id');

   define("_USER", "loketwpy");
   $data_array = $_SERVER + array("HTTP_X_REAL_IP" => $_SERVER['REMOTE_ADDR']);

   // create a new cURL resource
   $ch = curl_init();

   // set URL and other appropriate options
   curl_setopt($ch, CURLOPT_URL, 'https://secang.simeut.my.id:8080/tirtaintan-replika-publik/enquiry/'._USER.'/'.$_GET['data']);
   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_array)); 
   curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   echo curl_exec($ch);
	
   // close cURL resource, and free up system resources
   curl_close($ch);

   flush();

