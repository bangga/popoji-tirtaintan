<?php
   header('Content-Type: application/json');
   header('Cache-Control: no-cache');
   header('Access-Control-Allow-Origin: pdam.tirtaintan.co.id');

   include $_SERVER['DOCUMENT_ROOT']."/devel/logging.php";
   $log	   = new errorLog();

   define("_KODE", "devel");
   define("_USER", "loketalfa");
   define("_HOST", $_SERVER['REMOTE_ADDR']);

   // create a new cURL resource
   $ch = curl_init();

   // set URL and other appropriate options
   curl_setopt($ch, CURLOPT_URL, 'https://secang.simeut.my.id/tirtaintan-replika-publik/reversal/'._USER.'/'.$_GET['data']);
   curl_setopt($ch, CURLOPT_PORT, 8080);
   curl_setopt($ch, CURLOPT_POST, 1);
   curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_SERVER));
   curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '._HOST));
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   $raw_respon	= curl_exec($ch);

   $json_respon = json_decode($raw_respon);
   define("_TOKN", $json_respon->token);
   $log->logMess(bin2hex($raw_respon));

   echo $raw_respon;

   // close cURL resource, and free up system resources
   curl_close($ch);

   flush();

