<?php
	header('Content-Type: application/json');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: pdam.tirtaintan.co.id');

	// create a new cURL resource
	$ch = curl_init();

	// set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, 'https://secang.simeut.my.id/tirtaintan-replika-publik/enquiry/non-air/arindo/'.$_GET['data']);
        curl_setopt($ch, CURLOPT_PORT, 8080);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_SERVER));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '.$_SERVER['REMOTE_ADDR']));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	echo curl_exec($ch);
	
	// close cURL resource, and free up system resources
	curl_close($ch);

	flush();

