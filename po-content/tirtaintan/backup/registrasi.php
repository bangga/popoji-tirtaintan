<?php
if ($mod == "") {
    header('location:../../404.php');
} else {
    ?>
    <!-- 
    *******************************************************
            Include Header Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/header.php"; ?>


    <!-- 
    *******************************************************
            Main Content Template
    ******************************************************* 
    -->
    <section id="page">
        <header class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Registrasi Pemasangan</h1>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <ul id="navTrail">
                            <li><a href="<?= $website_url; ?>">Beranda</a></li>                            
                            <li id="navTrailLast"><strong>Registrasi Pemasangan</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <section id="registrasi" class="color0">   
            <div class="mb30"></div>
            <div class="container mb30">

                <div class="row">
                    <div class="col-sm-12 sep">
                        <h4>Registrasi Pemasangan</h4>
                        <p>Silahkan isi data Anda secara lengkap, jelas dan benar.</p>
                        <form class="registrasi-form" role="form" action="<?= $website_url; ?>/registrasi.php" method="post">
                            <div class="form-group">
                                <input type="text" name="reg_name" class="form-control" placeholder="Nama Lengkap" />
                            </div>
                            <div class="form-group">
                                <input type="text" name="reg_email" class="form-control" placeholder="E-mail" />
                            </div>
                            <div class="form-group">
                                <input type="text" name="reg_phone" class="form-control" placeholder="Telepon" />
                            </div>
                            <div class="form-group">
                                <input type="text" name="reg_identity" class="form-control" placeholder="Nomor KTP" />
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="reg_address" placeholder="Alamat" rows="6"></textarea>
                            </div>
			    <div class="form-group">
				<select class="form-control" name="reg_unit">
					<option value="00">&nbsp;Pilih Wilayah Pelayanan</option>
					<option value="01">&nbsp;Garut Kota</option>
					<option value="15">&nbsp;Tarogong Kidul</option>
					<option value="14">&nbsp;Tarogong Kaler</option>
					<option value="13">&nbsp;Cibunar</option>
					<option value="23">&nbsp;Mangkurakyat</option>
					<option value="05">&nbsp;Samarang</option>
					<option value="08">&nbsp;Banyuresmi</option>
					<option value="09">&nbsp;Leuwigoong</option>
					<option value="10">&nbsp;Cibatu</option>
					<option value="03">&nbsp;Karangpawitan</option>
					<option value="06">&nbsp;Cempaka</option>
					<option value="11">&nbsp;Wanaraja</option>
					<option value="23">&nbsp;Cilawu</option>
					<option value="04">&nbsp;Cisurupan</option>
					<option value="24">&nbsp;Cikajang</option>
					<option value="02">&nbsp;Pameungpeuk</option>
					<option value="18">&nbsp;Cibalong</option>
				</select>
			    </div>
                            <div class="form-group text-right">
                                <button class="btn btn-primary" name="send">Daftar</button>
                            </div>
                        </form>
                    </div>                    
                </div>

            </div>

        </section>
    </section>


    <!-- 
    *******************************************************
            Include Footer Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>
