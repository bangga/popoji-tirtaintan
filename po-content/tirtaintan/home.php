<?php
if ($mod == "") {
    header('location:../../404.php');
} else {
    ?>
    <!-- 
    *******************************************************
            Include Header Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/header.php"; ?>

    <!-- 
    *******************************************************
            Main Content Template
    ******************************************************* 
    -->

    <section class="carouselHome pt40 pb40 color2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel nekoDataOwl" data-neko_items="1" data-neko_singleitem="true" data-neko_paginationnumbers="true" data-neko_transitionstyle="backSlide">
                        <?php
                        $tableslider = new PoTable('post');
                        $sliders = $tableslider->findAllLimitByAnd(id_post, active, headline, 'Y', 'Y', DESC, '3');
                        foreach ($sliders as $slider) {
                            ?>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-5 col-md-offset-1">                                        
                                        <img src="<?= $website_url; ?>/po-content/po-upload/medium/medium_<?= $slider->picture; ?>" alt="PDAM Tirta Intan" class="img-responsive"/>
                                    </div>
                                    <div class="col-md-6 pt40">
                                        <h1><?= $slider->title; ?></h1>                                        
                                        <p><?= cuthighlight('post', $slider->content, '200'); ?>...</p>
                                    </div>
                                </div>
                            </div> 
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="content">
        <section class="largeQuote color1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center pt40 pb40">
                        <div class="span12 text-center">
                            <h1>PDAM Tirta Intan <strong>Garut</strong><br><small>Dengan Pelayanan Prima Menjadi PDAM Termaju dan Berdaya Saing</small></h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>    

        <section class="pt30 pb30">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <article class="iconBlocHeading">
                            <i class="icon-book iconRounded"></i>
                            <h2>
                                Berita Tirta Intan
                            </h2>

                            <p>Ikuti perkembangan Berita terbaru dan Informasi kegiatan PDAM Tirta Intan melalu website kami agar dapat mengetahui info terbaru.</p>
                            <a href="/category/berita" title="PDAM Tirta Intan Garut" class="btn btn-sm btn-primary">Selengkapnya</a> 
                        </article>
                    </div>
                    <div class="col-md-3">
                        <article class="iconBlocHeading">
                            <i class="icon-info iconRounded"></i>
                            <h2>
                                Info Tagihan
                            </h2>

                            <p>Informasi Pelanggan meliputi persyaratan menjadi pelanggan, peraturan pelanggan, golongan pelanggan, informasi tagihan penggunaan air dan pembayaran.</p>
                            <a href="/info-tagihan" title="PDAM Tirta Intan Garut" class="btn btn-sm btn-primary">Selengkapnya</a> 
                        </article>
                    </div>
                    <div class="col-md-3">
                        <article class="iconBlocHeading">
                            <i class="icon-money iconRounded"></i> 
                            <h2>
                                Simulasi Tarif
                            </h2>
                            <p>Simulasi perhitungan tarif dibuat untuk memberikan kemudahan kepada pelanggan yang ingin mengetahui perhitungan tarif tagihannya sendiri.</p>
                            <a href="/hitung-tarif" title="PDAM Tirta Intan Garut" class="btn btn-sm btn-primary">Selengkapnya</a> 
                        </article>
                    </div>
                    <div class="col-md-3">
                        <article class="iconBlocHeading">
                            <i class="icon-phone iconRounded"></i> 
                            <h2>
                                Pusat Bantuan
                            </h2>
                            <p>Komitmen dan kepedulian kami kepada pelanggan dengan semangat profesionalitas, handal dan memberikan pelayanan prima sebagai tujuan kami.</p>
                            <a href="#" title="PDAM Tirta Intan Garut" class="btn btn-sm btn-primary">Selengkapnya</a> 
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <!-- works -->
        <section class="imgHover clearfix cs-style-3 mb40">
            <div class="container">
                <div class="row">
                    <div class="span12 text-center mb40">
                        <h1>Galeri</h1>
                        <h2 class="subTitle">Dokumentasi Aktifitas PDAM Tirta Intan Garut</h2>
                    </div>
                </div>
            </div>

            <div class="row">

            </div>
            <div class="portfolio-items isotopeWrapper clearfix imgHover neko-hover-1" id="3">
                <?php
                $p = new Paging;
                $batas = 4;
                $posisi = $p->cariPosisi($batas);
                $tablegal = new PoTable('gallery');
                $gallerys = $tablegal->findAllLimit(id_gallery, "DESC", "$posisi,$batas");
                foreach ($gallerys as $gallery) {
                    $idalb = $gallery->id_album;
                    $tablecalb = new PoTable('album');
                    $currentCalb = $tablecalb->findBy(id_album, $idalb);
                    $currentCalb = $currentCalb->current();
                    if ($currentCalb->active == 'Y') {
                        ?>
                        <article class="col-sm-3 isotopeItem women">
                            <div class="">
                                <figure>
                                    <img alt="" src="<?= $website_url; ?>/po-content/po-upload/medium/medium_<?= $gallery->picture; ?>" class="img-responsive">
                                    <figcaption>
                                        <ul class="pinInfo">                                            
                                            <li><?= $gallery->title; ?></li>
                                        </ul>
                                        <div class="iconLinks">                                                
                                            <a href="<?= $website_url; ?>/po-content/po-upload/<?= $gallery->picture; ?>" class="image-link" title="Zoom" ><i class="icon-search"></i></a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </article>
                        <?php
                    }
                }
                ?>
            </div>        
            
        </section>
        <!-- works -->

        <section class="clearfix cs-style-3">
            <div class="container">
                <div class="row">
                    <div class="span12 text-center">
                        <h1>Partner</h1>
                        <h2 class="subTitle">Partner PDAM Tirta Intan Garut</h2>
                    </div>
                </div>
            </div>
        </section>    

        <section id="logos" class="pt40 pb40">
            <div class="container">              
                <address class="text-center">
                    <img src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/images/bri_logo.png" class="well"/>
                    <img src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/images/mandiri_syariah_logo.png" class="well"/>
                    <img src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/images/bukopin_logo.png" class="well"/>
                    <img src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/images/bjb_logo.png" class="well"/>
                    <img src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/images/pos_logo.png" class="well"/>   
                </address>            
            </div>
        </section>

    </section>

    <!-- 
    *******************************************************
            Include Footer Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>