<?php
if ($mod == "") {
    header('location:../../404.php');
} else {
    ?>
    <!-- 
    *******************************************************
            Include Header Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/header.php"; ?>


    <!-- 
    *******************************************************
            Main Content Template
    ******************************************************* 
    -->
    <div class="page">
        <header class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Galeri</h1>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <ul id="navTrail">
                            <li><a href="<?= $website_url; ?>">Beranda</a></li>                            
                            <li id="navTrailLast"><strong>Galeri</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <section id="content">
            <section class="pt30 pb30"> 
                <div class="container clearfix">
                    <div class="row">

                        <div class="portfolio-items  isotopeWrapper clearfix imgHover neko-hover-1" id="3">

                            <?php
                            $p = new Paging;
                            $batas = 9;
                            $posisi = $p->cariPosisi($batas);
                            $tablegal = new PoTable('gallery');
                            $gallerys = $tablegal->findAllLimit(id_gallery, "DESC", "$posisi,$batas");
                            foreach ($gallerys as $gallery) {
                                $idalb = $gallery->id_album;
                                $tablecalb = new PoTable('album');
                                $currentCalb = $tablecalb->findBy(id_album, $idalb);
                                $currentCalb = $currentCalb->current();
                                if ($currentCalb->active == 'Y') {
                                    ?>
                                    <article class="col-sm-4 isotopeItem women">
                                        <div class="">
                                            <figure>
                                                <img alt="" src="<?= $website_url; ?>/po-content/po-upload/medium/medium_<?= $gallery->picture; ?>" class="img-responsive">

                                                <figcaption>
                                                    <ul class="pinInfo">
                                                        <li><?= $gallery->title; ?></li>
                                                    </ul>
                                                    <div class="iconLinks">                                                
                                                        <a href="<?= $website_url; ?>/po-content/po-upload/<?= $gallery->picture; ?>" class="image-link" title="Zoom" ><i class="icon-search"></i></a>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                    </article>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <ul class="pagination">
                        <?php
                        $getpage = $val->validasi($_GET['page'], 'sql');
                        $jmldata = $tablegal->numRow();
                        $jmlhalaman = $p->jumlahHalaman($jmldata, $batas);
                        $linkHalaman = $p->navHalaman($getpage, $jmlhalaman, $website_url, "galeri", "page", "1");
                        echo "$linkHalaman";
                        ?>
                    </ul>
                </div>
            </section>
        </section>
    </section>
    <!-- content -->    
    </div>
    </div>
    <!-- 
    *******************************************************
            Include Footer Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>