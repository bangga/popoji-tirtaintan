<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>
<!-- 
*******************************************************
	Include Header Template
******************************************************* 
-->
<?php include_once "po-content/$folder/header.php"; ?>


<!-- 
*******************************************************
	Main Content Template
******************************************************* 
-->
    <section id="header">
        <header class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Simulasi Perhitungan Tarif</h1>
						<p>Perhitungan tarif berdasarkan pemakaian progresif</p>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <ul id="navTrail">
                            <li><a href="<?= $website_url; ?>">Beranda</a></li>                            
                            <li id="navTrailLast"><strong>Simulasi Perhitungan Tarif</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
	</section>
	
	<section id="content">
		<div class="mb30"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<input id="rek_pakai" type="text" name="rek_pakai" class="form-control" placeholder="Masukan Pemakaian Air (M3)" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<select class="form-control">
							<option id="00" value="00">&nbsp;Pilih Golongan Tarif</option>
							<option id="1A" value="1A">&nbsp;1A - Umum</option>
							<option id="1B" value="1B">&nbsp;1B - Khusus</option>
							<option id="2A" value="2A">&nbsp;2A - Rumah Tangga 1</option>
							<option id="2B" value="2B">&nbsp;2B - Rumah Tangga 2</option>
							<option id="2C" value="2C">&nbsp;2C - Rumah Tangga 3</option>
							<option id="2D" value="2D">&nbsp;2D - Intansi Pemerintah</option>
							<option id="3A" value="3A">&nbsp;3A - Niaga 1</option>
							<option id="3B" value="3B">&nbsp;3B - Niaga 2</option>
							<option id="3C" value="3C">&nbsp;3C - Niaga 3</option>
							<option id="3D" value="3D">&nbsp;3D - Industri Kecil</option>
							<option id="3E" value="3E">&nbsp;3E - Industri Besar</option>
						</select>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<input id="pel_no" type="text" name="pel_no" class="form-control" maxlength="6" placeholder="Atau 6 Digit Nomer Sambungan" />
					</div>
				</div>
				<div class="col-sm-12">
						<div class="form-group">
							<button class="btn btn-secondary" onclick="cariTarif()">Periksa</button>
						</div>
				</div>
			</div>
			<div class="row">
				<table id="tabel-tarif" class="table">
					<thead>
						<tr>
							<th class="hidden-xs">Segmentasi (M3)</th>
							<th>Pemakaian (M3)</th>
							<th class="hidden-xs">Harga Air (Rp/M3)</th>
							<th>Total (Rp)</th>
						</tr>
					</thead>
					<tbody id="body-tarif"></tbody>
					<tfoot id="foot-tarif"></tfoot>
				</table>
			</div>
		</div>
	</section>

<!-- 
*******************************************************
	Include Footer Template
******************************************************* 
-->
<?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>

    <!-- number formater -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/numeral/jshashtable-2.1.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/numeral/jquery.numberformatter-1.2.3.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/numeral/numeral.min.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/numeral/numeral.de-de.js"></script>

	<script>
		function cariTarif(){
			var dataTemp = {};
			var dataProc = document.getElementsByClassName('form-control');
			var dataFeed = {};

			if(typeof(localStorage.tirtaintan)=='string'){
				dataTemp = JSON.parse(localStorage.tirtaintan);
			}
			else{
				dataTemp.rek_pakai = 0;
			}
			
			if(parseInt(dataProc[0].value)>0){
				dataTemp.rek_pakai = dataProc[0].value;
			}
			
			if(dataTemp.rek_pakai>0){
				if(dataProc[2].value.length==6){
					var xmlhttp = new XMLHttpRequest();
					var url 	= "http://api.tirtaintan.co.id/pelanggan/www/view_rinci.php?pel_no=" + dataProc[2].value;

					xmlhttp.onreadystatechange = function() {
						if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
							dataFeed = JSON.parse(xmlhttp.responseText);
							if(dataFeed.data.length>0){
								dataTemp.pel_no 	= dataFeed.data[0].client_id;
								dataTemp.gol_kel	= dataFeed.data[0].client_grup;
								localStorage.setItem('tirtaintan', JSON.stringify(dataTemp));
								document.location.href  = '/hitung-tarif';
							}
						}
					};
					xmlhttp.open('GET', url, true);
					xmlhttp.send();
				}
				else{
					dataTemp.gol_kel 	= dataProc[1].value;
					localStorage.setItem('tirtaintan', JSON.stringify(dataTemp));
					document.location.href  = '/hitung-tarif';
				}
			}
			else{
				alert('Volume pemakaian air belum diisi');
			}
		}
	</script>

	<script>
		(function() {
			var dataTemp 	= {};
			var inHTML1		= "";
			var inHTML2		= "";
			var inHTML3		= "";
			var inHTML4		= "";
			var inHTML5		= "";
			var tar_sd1		= 0;
			var tar_sd2		= 0;
			var tar_sd3		= 0;
			var tar_sd4		= 0;
			var tar_hd1		= 0;
			var tar_hd2		= 0;
			var tar_hd3		= 0;
			var tar_hd4		= 0;
			var rek_pakai	= 0;
			var tot_pakai	= 0;
			var tot_harga	= 0;
			if(typeof(localStorage.tirtaintan)=='string'){
				dataTemp = JSON.parse(localStorage.tirtaintan);
				document.getElementById(dataTemp.gol_kel).setAttribute('selected', true);
				if(typeof(dataTemp.rek_pakai)=='string'){
					document.getElementById('rek_pakai').setAttribute('placeholder', 'Pemakaian Air ' + dataTemp.rek_pakai + ' (M3)');
				}
				if(typeof(dataTemp.pel_no)=='string'){
					document.getElementById('pel_no').setAttribute('placeholder', 'Nomer Sambungan ' + dataTemp.pel_no);
				}
				rek_pakai	= parseInt(dataTemp.rek_pakai);
				
				if(rek_pakai<10){
					rek_pakai = 10;
				}
				tot_pakai	= rek_pakai;

				var dataArray = {filter : [{name : "gol_kode", value: dataTemp.gol_kel}]};
				jQuery.post("https://pdam.tirtaintan.co.id/report-billing/table/json/kode-tarif", dataArray).done(function(data) {
					console.log(data);

				switch(dataTemp.gol_kel){
					case '1A':
						tar_sd1 = 30;
						tar_sd2 = 100;
						tar_sd3 = 100;
						tar_sd4 = 100;
						tar_hd1 = 1950;
						tar_hd2 = 3120;
						tar_hd3 = 3120;
						tar_hd4 = 3120;
						break;
					case '3E':
						tar_sd1 = 30;
						tar_sd2 = 100;
						tar_sd3 = 100;
						tar_sd4 = 100;
						tar_hd1 = 14000;
						tar_hd2 = 21000;
						tar_hd3 = 21000;
						tar_hd4 = 24500;
						break;
					default:
                                                tar_sd1 = parseInt(data[0].tar_sd1);
                                                tar_sd2 = parseInt(data[0].tar_sd2);
                                                tar_sd3 = parseInt(data[0].tar_sd3);
                                                tar_sd4 = parseInt(data[0].tar_sd4);
                                                tar_hd1 = parseInt(data[0].tar_1);
                                                tar_hd2 = parseInt(data[0].tar_2);
                                                tar_hd3 = parseInt(data[0].tar_3);
                                                tar_hd4 = parseInt(data[0].tar_4);
				}


				if(rek_pakai>tar_sd3){
					inHTML4 = '<tr>' +
						'<td class="hidden-xs">>' + tar_sd3 + '</td>' +
						'<td class="text-right">' + (rek_pakai-tar_sd3) + '</td>' +
						'<td class="hidden-xs text-center">' + jQuery.formatNumber(tar_hd4, {format:'#,###', locale:'de'}) + '</td>' +
						'<td class="text-right">' + jQuery.formatNumber(tar_hd4*(rek_pakai-tar_sd3), {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>';
					tot_harga = tar_hd4*(rek_pakai-tar_sd3);
					rek_pakai = tar_sd3;
				}
				if(rek_pakai>tar_sd2){
					inHTML3 = '<tr>' +
						'<td class="hidden-xs">' + (tar_sd2+1) + ' - ' + tar_sd3 + '</td>' +
						'<td class="text-right">' + (rek_pakai-tar_sd2) + '</td>' +
						'<td class="hidden-xs text-center">' + jQuery.formatNumber(tar_hd3, {format:'#,###', locale:'de'}) + '</td>' +
						'<td class="text-right">' + jQuery.formatNumber(tar_hd3*(rek_pakai-tar_sd2), {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>';
					tot_harga = tot_harga + tar_hd3*(rek_pakai-tar_sd2);
					rek_pakai = tar_sd2;
				}
				if(rek_pakai>tar_sd1){
					inHTML2 = '<tr>' +
						'<td class="hidden-xs">' + (tar_sd1+1) + ' - ' + tar_sd2 + '</td>' +
						'<td class="text-right">' + (rek_pakai-tar_sd1) + '</td>' +
						'<td class="hidden-xs text-center">' + jQuery.formatNumber(tar_hd2, {format:'#,###', locale:'de'}) + '</td>' +
						'<td class="text-right">' + jQuery.formatNumber(tar_hd2*(rek_pakai-tar_sd1), {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>';
					tot_harga = tot_harga + tar_hd2*(rek_pakai-tar_sd1);
					rek_pakai = tar_sd1;
				}
				inHTML1 	= '<tr>' +
						'<td class="hidden-xs">1 - ' + tar_sd1 + '</td>' +
						'<td class="text-right">' + rek_pakai + '</td>' +
						'<td class="hidden-xs text-center">' + jQuery.formatNumber(tar_hd1, {format:'#,###', locale:'de'}) + '</td>' +
						'<td class="text-right">' + jQuery.formatNumber(tar_hd1*rek_pakai, {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>';
				tot_harga	= tot_harga + tar_hd1*rek_pakai;
				// tabel footer : AIR
				inHTML5 	= '<tr>' +
						'<td class="hidden-xs"></td>' +
						'<td class="text-right">' + tot_pakai + '</td>' +
						'<td class="hidden-xs"></td>' +
						'<td class="text-right">' + jQuery.formatNumber(tot_harga, {format:'#,###', locale:'de'}) + '</td>' +
					'</tr>';

				document.getElementById('body-tarif').innerHTML = inHTML1 + inHTML2 + inHTML3 + inHTML4;
                                // tabel footer : ADM
                                inHTML5         = inHTML5 + '<tr>' +
                                                '<td class="hidden-xs"></td>' +
                                                '<td class="text-right">ADM</td>' +
                                                '<td class="hidden-xs"></td>' +
                                                '<td class="text-right">' + jQuery.formatNumber(12000, {format:'#,###', locale:'de'}) + '</td>' +
                                        '</tr>';
                                inHTML5         = inHTML5 + '<tr>' +
                                                '<td class="hidden-xs"></td>' +
                                                '<td class="text-right">Total</td>' +
                                                '<td class="hidden-xs"></td>' +
                                                '<td class="text-right">' + jQuery.formatNumber((12000+tot_harga), {format:'#,###', locale:'de'}) + '</td>' +
                                        '</tr>';
                                document.getElementById('foot-tarif').innerHTML = inHTML5;

                                });
			}
		})();
	</script>
