<?php
if ($mod == "") {
    header('location:../../404.php');
} else {
    ?>
    <aside class="col-md-4">        
        <section class="widget tagCloud">
            <h3>Post Tag</h3>
            <?php
            $tabletag = new PoTable('tag');
            $tags = $tabletag->findAllLimit(id_tag, DESC, '20');
            foreach ($tags as $tag) {
                ?>
                <a href="<?= $website_url; ?>/search-result/<?= $tag->tag_title; ?>" class="w2" title="<?= $tag->tag_title; ?>"><?= $tag->tag_title; ?></a>
            <?php } ?>       
        </section>

        <section class="widget tagCloud">
            <h3>Post Komentar</h3>
            <?php if ($mod == "home" OR $mod == "detailpost") { ?>

                <?php
                $tablesidecat = new PoTable('category');
                $tablejmlpost = new PoTable('post');
                $sidecats = $tablesidecat->findAllRand();
                foreach ($sidecats as $sidecat) {
                    $tablejmlposts = $tablejmlpost->numRowByAnd(id_category, $sidecat->id_category, active, 'Y');
                    ?>
                    <a href="<?= $website_url; ?>/category/<?= $sidecat->seotitle; ?>" title="<?= $sidecat->title; ?>"><?= $sidecat->title; ?> (<?= $tablejmlposts; ?>)</a>
                <?php } ?>
            <?php } ?>                        
        </section>

        <section class="widget tagCloud">
            <h3>Post Terbaru</h3>
            <?php if ($mod != "gallery" AND $mod != "contact" AND $mod != "login" AND $mod != "register") { ?>
                
                    <?php
                    $tablerec = new PoTable('post');
                    $recs = $tablerec->findAllLimitBy(id_post, active, 'Y', DESC, '1');
                    foreach ($recs as $rec) {
                        $validrec = $rec->id_category;
                        $tablecatrec = new PoTable('category');
                        $currentCatrec = $tablecatrec->findBy(id_category, $validrec);
                        $currentCatrec = $currentCatrec->current();
                        ?>
                        <div>
                            <a href="<?php echo "$website_url/detailpost/$rec->seotitle"; ?>" title="<?= $rec->title; ?>"><?= cuthighlight('title', $rec->title, '40'); ?>...</a>
                            <?php
                            $tablerecb = new PoTable('post');
                            $recbs = $tablerecb->findAllLimitBy(id_post, active, 'Y', DESC, '1,5');
                            foreach ($recbs as $recb) {
                                ?>
                                <a href="<?php echo "$website_url/detailpost/$recb->seotitle"; ?>" title="<?= $recb->title; ?>"><?= cuthighlight('title', $recb->title, '40'); ?>...</a>
                            <?php } ?>
                        </div>

                    <?php } ?>
                
            <?php } ?>                        
        </section>                

    </aside>    
<?php } ?>