<?php
if ($mod == "") {
    header('location:../../404.php');
} else {
    ?>
    <!-- 
    *******************************************************
            Include Header Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/header.php"; ?>


    <!-- 
    *******************************************************
            Main Content Template
    ******************************************************* 
    -->
    <?php
    $title = $val->validasi($_GET['idp'], 'xss');
    $tablepag = new PoTable('pages');
    $currentPag = $tablepag->findByAnd(seotitle, $title, active, 'Y');
    $currentPag = $currentPag->current();
    $idpag = $currentPag->id_pages;
    $content = $currentPag->content;
    $content = html_entity_decode($content);
    ?>
    <?php if ($currentPag != "0") { ?>
        <header class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1><?= $currentPag->title; ?></h1>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <ul id="navTrail">
                            <li><a href="<?= $website_url; ?>">Beranda</a></li>                            
                            <li id="navTrailLast"><strong><?= $currentPag->title; ?></strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <section id="aboutUs" class="pt30 pb30 ">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div style="opacity: 1; display: block;" class="noOwlButtons owl-carousel nekoDataOwl owl-theme" data-neko_items="1" data-neko_singleitem="true" data-neko_paginationnumbers="true">
                            <div class="owl-wrapper-outer">
                                <div style="width: 3330px; left: 0px; display: block;" class="owl-wrapper">
                                    <div style="width: 555px;" class="owl-item">
                                        <img src="<?= $website_url; ?>/po-content/po-upload/<?= $currentPag->picture; ?>" class="img-responsive" alt="PDAM Tirta Intan">
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h2><?= $currentPag->title; ?></h2>                        
                        <p>
                            <?= $content; ?>
                        </p>
                        
                    </div>
                </div>
            </div>
        </section>
        <?php
    } else {
        header('location:' . $website_url . '/404.php');
    }
    ?>


    <!-- 
    *******************************************************
            Include Footer Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>