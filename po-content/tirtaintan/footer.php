<?php
if ($mod == "") {
    header('location:../../404.php');
} else {
    ?>

    <footer id="footerWrapper">
        <section id="mainFooter">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="footerWidget">
                            <img src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/images/logo.png" alt="PDAM Tirta Intan" alt="PDAM Tirta Intan Garut" id="footerLogo"/>                            
                            <p><a href="http://www.tirtaintan.co.id/" title="PDAM Tirta Intan Garut">PDAM Tirta Intan</a> Kabupaten Garut dibentuk pengelola sistem air bersih dilaksanakan oleh Seksi Air Minum yang berada dibawah Dinas Pekerjaan Umum Kabupaten Daerah Tingkat II Kabupaten Garut.</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footerWidget">
                            <h3>Alamat</h3>
                            <address>
                                <p>
                                    <i class="icon-location"></i>&nbsp; Jl. Raya Bayongbong Km 3<br>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kp. Gandasari – Cilawu<br>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kabupaten Garut <br>
                                    <i class="icon-phone"></i>&nbsp;&nbsp;(0262) 2248250 <br>
                                    <i class="icon-mail-alt"></i>&nbsp; <a href="mailto:pdamgarut@tirtaintan.co.id">pdamgarut@tirtaintan.co.id</a>
                                </p>
                            </address>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footerWidget">
                            <h3>Ikuti kami di jejaring sosial</h3>
                            <ul class="socialNetwork">
                                <li><a data-original-title="follow me on Facebook" href="#" class="tips" title=""><i class="icon-facebook-1 iconRounded"></i></a></li>
                                <li><a data-original-title="follow me on Twitter" href="#" class="tips" title=""><i class="icon-twitter-bird iconRounded"></i></a></li>
                                <li><a data-original-title="follow me on Google+" href="#" class="tips" title=""><i class="icon-gplus-1 iconRounded"></i></a></li>
                                <li><a data-original-title="follow me on Linkedin" href="#" class="tips" title=""><i class="icon-linkedin-1 iconRounded"></i></a></li>
                                <li><a data-original-title="follow me on Dribble" href="#" class="tips" title=""><i class="icon-dribbble iconRounded"></i></a></li>
                            </ul>     
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="footerRights">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p>Copyright © 2016 <a href="http://www.tirtaintan.co.id/" target="blank">PDAM Tirta Intan</a> / All rights reserved.</p>
                    </div>
                </div>
            </div>
        </section>
    </footer>

    </div>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js/modernizr-2.6.1.min.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/respond/respond.min.js"></script>        
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/jquery-ui/jquery-ui-1.8.23.custom.min.js"></script>
    <!-- third party plugins  -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/easing/jquery.easing.1.3.js"></script>
    <!-- carousel -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/owl.carousel/owl-carousel/owl.carousel.min.js"></script>
    <!-- pop up -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/magnific-popup/jquery.magnific-popup.min.js"></script>
    
    <!-- sharrre -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/jquery.sharrre-1.3.4/jquery.sharrre-1.3.4.min.js"></script>
    <!-- form -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/neko-contact-ajax-plugin/js/jquery.form.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/neko-contact-ajax-plugin/js/jquery.validate.min.js"></script>
    <!-- parallax -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/parallax/js/jquery.stellar.min.js"></script>
    <!-- Background Video -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/ytplayer/jquery.mb.YTPlayer.js"></script>
    <!-- appear -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/appear/jquery.appear.js"></script>
    <!-- Custom  -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js/custom.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/jquery-cookie/jquery.cookie.js"></script>    

    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- isotope -->
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/isotope/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/isotope/jquery.isotope.sloppy-masonry.min.js"></script>

    </body>
    </html>
<?php } ?>