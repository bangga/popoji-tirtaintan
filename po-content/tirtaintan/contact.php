<?php
if ($mod == "") {
    header('location:../../404.php');
} else {
    ?>
    <!-- 
    *******************************************************
            Include Header Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/header.php"; ?>


    <!-- 
    *******************************************************
            Main Content Template
    ******************************************************* 
    -->
    <script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        function initialize()
        {
            var mapDiv = document.getElementById('map');
            var map = new google.maps.Map(mapDiv, {
                center: new google.maps.LatLng(-6.241783, 106.836029),
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false
            });
            new google.maps.Marker({
                position: new google.maps.LatLng(-6.241783, 106.836029),
                map: map
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>    

    <section id="page">
        <header class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Kontak</h1>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <ul id="navTrail">
                            <li><a href="<?= $website_url; ?>">Beranda</a></li>                            
                            <li id="navTrailLast"><strong>Kontak</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <section class="contact-map" id="map"></section>
        <section id="contact" class="color0">
            
            <div id="mapWrapper" class="mb30"></div>
            <div class="container mb30"  data-nekoanim="fadeIn" data-nekodelay="400">

                <div class="row">
                    <div class="col-sm-8 sep">
                    <h4>Tetap terhubung dengan kami</h4>
                    <p>Silahkan kontak ke alamat, email, fax, no telepon atau kontak kami di bawah ini</p>
                    <form class="contact-form" role="form" action="<?= $website_url; ?>/contact.php" method="post">
                        <div class="form-group">
                            <input type="text" name="name_contact" class="form-control" placeholder="Name" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="email_contact" class="form-control" placeholder="E-mail" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="subject_contact" class="form-control" placeholder="Subject" />
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message_contact" placeholder="Message" rows="6"></textarea>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-primary" name="send">Send</button>
                        </div>
                    </form>
                </div>
                    <div class="col-sm-offset-1 col-sm-3">
                        <h4>Alamat:</h4>
                        <address>
                            Jl. Raya Bayongbong Km 3<br/>
                            Kp. Gandasari – Cilawu<br/>
                            Kabupaten Garut  <br/>
                        </address>
                        <h4>Telepon:</h4>
                        <address>
                            (0262) 2248250 <br/>
                        </address>
                    </div>
                    
                </div>
                
            </div>

        </section>
    </section>


    <!-- 
    *******************************************************
            Include Footer Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>