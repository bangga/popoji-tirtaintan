<?php
if ($mod == "") {
    header('location:../../404.php');
} else {
    ?>
    <!-- 
    *******************************************************
            Include Header Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/header.php"; ?>

    <!-- 
    *******************************************************
            Main Content Template
    ******************************************************* 
    -->
    <?php
    $title = $val->validasi($_GET['id'], 'xss');
    $detail = new PoTable();
    $currentDetail = $detail->findManualQuery($tabel = "post,users,category", $field = "", $condition = "WHERE users.id_user = post.editor AND category.id_category = post.id_category AND category.active = 'Y' AND post.active = 'Y' AND post.seotitle = '" . $title . "'");
    $currentDetail = $currentDetail->current();
    $idpost = $currentDetail->id_post;

    if ($currentDetail > 0) {
        $tabledpost = new PoTable('post');
        $currentDpost = $tabledpost->findByPost(id_post, $idpost);
        $currentDpost = $currentDpost->current();

        $contentdet = html_entity_decode($currentDetail->content);
        $biodet = html_entity_decode($currentDetail->bio);

        $tabledcat = new PoTable('category');
        $currentDcat = $tabledcat->findBy(id_category, $currentDetail->id_category);
        $currentDcat = $currentDcat->current();
        $tableuser = new PoTable('users');
        $currentUser = $tableuser->findBy(id_user, $aRow['editor']);
        $currentUser = $currentUser->current();

        $p = new Paging;
        $batas = 5;
        $posisi = $p->cariPosisi($batas);
        $tabledcom = new PoTable('comment');
        $composts = $tabledcom->findAllLimitByAnd(id_comment, id_post, active, "$idpost", "Y", "ASC", "$posisi,$batas");
        $totaldcom = $tabledcom->numRowByAnd(id_post, $idpost, active, 'Y');

        mysql_query("UPDATE post SET hits = $currentDetail->hits+1 WHERE id_post = '" . $idpost . "'");
        ?>
        <!-- Breadcrumb -->
        <section id="page">
            <header class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <h1><?= $currentDpost->title; ?></h1>                            
                        </div>
                        <div class="col-sm-3 hidden-xs">
                            <ul id="navTrail">
                                <li><a href="<?= $website_url; ?>">Beranda</a></li>  
                                <li><a href="<?= $website_url; ?>/category/berita">Berita</a></li>  
                                <li id="navTrailLast"><strong>Detail</strong></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>


            <section id="content" class="pt30 mb30">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">

                            <article class="post clearfix">
                                <div class="postPic">
                                    <div class="imgBorder mb15">
                                        <a href="blog-post.html"><img src="<?= $website_url; ?>/po-content/po-upload/<?= $currentDetail->picture; ?>" alt="" class="img-responsive"></a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="postMeta col-sm-1 col-xs-2">
                                        <i class="iconRounded icon-camera"></i>
                                    </div>

                                    <section class="col-sm-11 col-xs-10">
                                        <h2><a href="blog-post.html"><?= $currentDpost->title; ?></a></h2>
                                        <ul class="entry-meta">
                                            <li class="entry-date"><a href="#"><i class="icon-pin"></i>&nbsp;<?= tgl_indo($currentDetail->date); ?></a></li>
                                            <li class="entry-author"><a href="#"><i class="icon-male"></i>&nbsp;<?= $currentDetail->nama_lengkap; ?></a></li>
                                            <li class="entry-date"><a href="#"><i class="icon-eye"></i>&nbsp;<?= $currentDetail->hits; ?> Views</a></li>
                                            <li class="entry-comments"><a href="#"><i class="icon-comment-1"></i>&nbsp;<?= $totaldcom; ?> Komentar</a></li>
                                        </ul>
                                        <p>
                                            <?= $contentdet; ?>
                                        </p>                                        
                                </div>
                            </article>

                            <hr>
                            <div class="row page_margin_top_section" id="comment-box">
                                <h3 class="commentNumbers">Tinggalkan Komentar</h3>
                                <p class="padding_top_30">Your email address will not be published. Required fields are marked with *</p>
                                <form method="post" action="<?= $website_url; ?>/po-postcom.php" method="post">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input class="form-control" name="name" id="name" placeholder="Name *" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control" name="email" id="email" placeholder="Email *" type="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="website">Website</label>
                                        <input class="form-control" name="url" id="email" placeholder="Website *" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="comment">Comment</label>
                                        <textarea cols="5" class="form-control" rows="5" name="comment" id="comment" placeholder="Comment *"></textarea>
                                    </div>
                                    <br />
                                    
                                    <div class="g-recaptcha" data-sitekey="6LckEgETAAAAAPdqrQSY_boMDLZRL1vpkAatVqKf"></div>
                                    
                                    <input type="hidden" name="id" value="<?= $idpost; ?>" />
                                    <input type="hidden" name="seotitle" value="<?= $currentDpost->seotitle; ?>" />
                                    <br />
                                    <input type="submit" value="POST COMMENT" class="btn btn-primary"/>                                        
                                </form>                                    
                            </div>

                            <hr>
                            <section class="clearfix comments pt30">
                                                           
                                <div class="row page_margin_top_section" id="comment-list">
                                    <h4 class="box_header"><?= $totaldcom; ?> Komentar</h4>
                                    <?php if ($totaldcom > 0) { ?>                                        
                                        <?php
                                        foreach ($composts as $compost) {
                                            $comcontent = nl2br($compost->comment);
                                            ?>
                                            <div class="media"> <a class="pull-left" href="#">
                                                    <div class="imgWrapper"><img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=50" alt=""></div>
                                                </a>
                                                <div class="media-body">
                                                    <div class="clearfix">
                                                        <h4 class="media-heading"><?php if ($compost->url != '') { ?>
                                                                <h5><a class="author" href="<?= addhttp($compost->url); ?>" target="_blank"><?= $compost->name; ?></a></h5>
                                                            <?php } else { ?>
                                                                <h5><a class="author" href="#"><?= $compost->name; ?></a></h5>
                                                            <?php } ?></h4>
                                                        <div class="commentInfo"> <span><?php echo "$compost->date | $compost->time"; ?></span></div>
                                                    </div>
                                                    <?= autolink($comcontent); ?>
                                                </div>
                                            </div>                                                                                
                                        <?php } ?>

                                        <ul class="pagination page_margin_top_section">                                            
                                            <?php
                                            $getpage = $val->validasi($_GET['page'], 'sql');
                                            $jmldata = $tabledcom->numRowByAnd(id_post, $idpost, active, 'Y');
                                            $jmlhalaman = $p->jumlahHalaman($jmldata, $batas);
                                            $linkHalaman = $p->navHalaman($getpage, $jmlhalaman, $website_url, "detailpost", $currentDpost->seotitle, "1");
                                            echo "$linkHalaman";
                                            ?>
                                        </ul>
                                    <?php } ?>
                                </div>

                            </section>

                        </div>
                        <!-- Sidebar -->
                        <?php include_once "po-content/$folder/sidebar.php"; ?>
                        
                    </div><!-- row -->
                </div><!-- container -->
            </section>        


        <?php
    } else {
        header('location:' . $website_url . '/404.php');
    }
    ?>


    <!-- 
    *******************************************************
            Include Footer Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>