<?php
if ($mod == "") {
    header('location:../../404.php');
} else {
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
        <head>            
            <title><?php include "title.php"; ?></title>
            <meta charset="utf-8" />
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="robots" content="index, follow" />
            <meta name="description" content="<?php include "meta-desc.php"; ?>" />
            <meta name="keywords" content="<?php include "meta-key.php"; ?>" />
            <meta http-equiv="Copyright" content="pdam tirta intan" />
            <meta name="author" content="Dadang Nurjaman" />
            <meta http-equiv="imagetoolbar" content="no" />
            <meta name="language" content="Indonesia" />
            <meta name="revisit-after" content="7" />
            <meta name="webcrawlers" content="all" />
            <meta name="rating" content="general" />
            <meta name="spiders" content="all" />

            <?php include "meta-social.php"; ?>

            <meta name="viewport" content="width=device-width" />

            <link type="text/css" rel="stylesheet" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/bootstrap/css/bootstrap.min.css" />

            <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400' rel='stylesheet' type='text/css' />        

            <link rel="stylesheet" type="text/css" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/magnific-popup/magnific-popup.css" />

            <link rel="stylesheet" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/owl.carousel/owl-carousel/owl.carousel.css" />
            <link rel="stylesheet" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/owl.carousel/owl-carousel/owl.theme.css" />

            <link rel="stylesheet" type="text/css" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/appear/nekoAnim.css" />

            <link type="text/css" rel="stylesheet" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/ytplayer/YTPlayer.css" />	

            <link type="text/css" rel="stylesheet" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/font-icons/custom-icons/css/custom-icons.css" />
            <link type="text/css" rel="stylesheet" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/font-icons/custom-icons/css/custom-icons-ie7.css" />

            <link type="text/css" rel="stylesheet" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/css/layout.css" />
            <link type="text/css" id="colors" rel="stylesheet" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/css/sea-green.css" />
            <link type="text/css" rel="stylesheet" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/css/custom.css" />
			
			<!-- DataTable -->
            <link type="text/css" rel="stylesheet" href="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/css/dataTables.bootstrap.css" />

            <link rel="shortcut icon" href="<?= $website_url; ?>/<?= $favicon; ?>" />

            <script src="https://www.google.com/recaptcha/api.js"></script>

            <!--
            <link rel="shortcut icon" href="<?= $website_url; ?>/<?= $favicon; ?>" />
            -->

            <script src="https://www.google.com/recaptcha/api.js"></script>
        </head>
        <body class="activateAppearAnimation">
            <div class="globalWrapper">
                <header class="navbar-fixed-top">                
                    <div id="mainHeader" role="banner">
                        <div class="container">
                            <nav class="navbar navbar-default" role="navigation">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>                                    
                                    <a class="navbar-brand" href="<?= $website_url; ?>">
                                        <img src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/images/logo.png" alt="PDAM Tirta Intan"/>
                                    </a>
                                </div>
                                <div class="collapse navbar-collapse" id="mainMenu">
                                    <ul class="nav navbar-nav pull-right">
                                        <li class="primary"><a href="<?= $website_url; ?>" class="firstLevel active">Beranda</a></li>
                                        <li class="primary">
                                            <a href="#" class="firstLevel hasSubMenu" >Profil <b class="caret"></b></a>
                                            <ul class="subMenu">                                            
                                                <li><a href="<?= $website_url; ?>/pages/biografi-direksi">Biografi Direksi</a></li>
                                                <li><a href="<?= $website_url; ?>/pages/tentang-pdam">Tentang PDAM</a></li>
                                                <li><a href="<?= $website_url; ?>/pages/visi-misi">Visi dan Misi</a></li>
                                                <li><a href="<?= $website_url; ?>/pages/struktur-organisasi">Struktur Organisasi</a></li>
                                                <li><a href="<?= $website_url; ?>/pages/core-values">Core Values</a></li>
                                                <li><a href="<?= $website_url; ?>/pages/peta-wilayah-langganan">Peta Wilayah Langganan</a></li>
                                                <li><a href="<?= $website_url; ?>/pages/kapasitas-produksi">Kapasitas Produksi</a></li>
                                                <li><a href="<?= $website_url; ?>/pages/sambungan-langganan">Sambungan Langganan</a></li>                                
                                                <li class="last"><a href="<?= $website_url; ?>/pages/produk-hukum">Produk Hukum</a></li>                                                                
                                            </ul>
                                        </li>                        
                                        <li class="primary">
                                            <a href="#" class="firstLevel hasSubMenu" >Perkembangan Usaha <b class="caret"></b></a>
                                            <ul class="subMenu">                                            
                                                <li><a href="<?= $website_url; ?>/pages/aspek-pelayanan">Aspek Pelayanan</a></li>
                                                <li><a href="<?= $website_url; ?>/pages/aspek-teknis">Aspek Teknis</a></li>
                                                <li><a href="<?= $website_url; ?>/pages/aspek-manajemen">Aspek Manajemen</a></li>                                                           
                                                <li class="last"><a href="<?= $website_url; ?>/pages/aspek-keuangan">Aspek Keuangan</a></li>                                                                
                                            </ul>
                                        </li>                        
                                        <li class="primary">
                                            <a href="#" class="firstLevel hasSubMenu" >Pelayanan Langganan <b class="caret"></b></a>
                                            <ul class="subMenu">                                            
                                                <li><a href="<?= $website_url; ?>/registrasi" class="firstLevel last">Registrasi</a></li>
												<li><a href="<?= $website_url; ?>/status-registrasi" class="firstLevel last">Info Registrasi</a></li>
												<li><a href="<?= $website_url; ?>/info-tagihan" class="firstLevel last">Info Tagihan</a></li>
												<li><a href="<?= $website_url; ?>/hitung-tarif" class="firstLevel last">Simulasi Tarif</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="<?= $website_url; ?>/category/berita" class="firstLevel last">Berita</a></li>
                                        <li><a href="<?= $website_url; ?>/gallery" class="firstLevel last">Galeri</a></li>
                                        <li><a href="<?= $website_url; ?>/contact" class="firstLevel last">Kontak</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </header>                
            <?php } ?>