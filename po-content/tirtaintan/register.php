<?php if ($mod==""){
	header('location:../../404.php');
}else{
	if ($member_register == "Y"){
?>
<!-- 
*******************************************************
	Include Header Template
******************************************************* 
-->
<?php include_once "po-content/$folder/header.php"; ?>


<!-- 
*******************************************************
	Main Content Template
******************************************************* 
-->
        <div class="page">
		<header class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Register Member</h1>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <ul id="navTrail">
                            <li><a href="<?= $website_url; ?>">Beranda</a></li>                            
                            <li id="navTrailLast"><strong>Register Member</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>      

<div class="row">
                    <div class="col-sm-8 sep">
                    <h4>Tetap terhubung dengan kami</h4>
                    <p>Silahkan kontak ke alamat, email, fax, no telepon atau kontak kami di bawah ini</p>
                    <form class="contact-form" role="form" action="<?= $website_url; ?>/contact.php" method="post">
                        <div class="form-group">
                            <input type="text" name="name_contact" class="form-control" placeholder="Name" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="email_contact" class="form-control" placeholder="E-mail" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="subject_contact" class="form-control" placeholder="Subject" />
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message_contact" placeholder="Alamat" rows="6"></textarea>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-primary" name="send">Send</button>
                        </div>
                    </form>
                </div>
                    <div class="col-sm-offset-1 col-sm-3">
                        <h4>Alamat:</h4>
                        <address>
                            Jl. Raya Bayongbong Km 3<br/>
                            Kp. Gandasari – Cilawu<br/>
                            Kabupaten Garut  <br/>
                        </address>
                        <h4>Telepon:</h4>
                        <address>
                            (0262) 2248250 <br/>
                        </address>
                    </div>
                    
                </div>
		
            <div class="page_layout clearfix">
                <div class="divider_block clearfix">
                    <hr class="divider first"><hr class="divider subheader_arrow"><hr class="divider last">
                </div>
                <div class="row">
                    <div class="column column_2_3">
                        <p class="padding_top_30">Daftarkan profilmu dan share segala sesuatu tentangmu sekarang.</p>
                        <form class="margin_top_15" name="register-form" method="post" action="<?=$website_url;?>/po-admin/actregister.php" autocomplete="off">
                            <fieldset>
								<div class="block">
                                    <label>Username</label><br /><br />
                                    <input class="text_input" type="text" name="username" id="username" placeholder="Username" style="width:80%;" /><br /><br />
                                </div>
                            </fieldset>
                            <fieldset>
								<div class="block">
                                    <label>Email</label><br /><br />
                                    <input class="text_input" type="text" name="email" id="email" placeholder="Email" style="width:80%;" /><br /><br />
                                </div>
                            </fieldset>
                            <fieldset>
								<div class="block">
                                    <label>Password</label><br /><br />
                                    <input class="text_input" type="password" name="password" id="password" placeholder="Password" style="width:80%;" /><br /><br />
                                </div>
                            </fieldset>
                            <fieldset>
								<div class="block">
                                    <label>Retype Password</label><br /><br />
                                    <input class="text_input" type="password" name="re-password" id="re-password" placeholder="Retype Password" style="width:80%;" /><br /><br />
                                </div>
                            </fieldset>
                            <fieldset>
                                <label>Sudah punya akun sebelumnya ? Klik <a href="<?=$website_url;?>/login" title="Login Member">di sini!</a></label><br /><br />
                            </fieldset>
                            <fieldset>
                                <input class="more active" type="submit" value="Register Member" name="submit" />
                            </fieldset>
                        </form>
                    </div>                   
                </div>
            </div>
        </div>


<!-- 
*******************************************************
	Include Footer Template
******************************************************* 
-->
<?php include_once "po-content/$folder/footer.php"; ?>
<?php
	}else{
		header('location:404.php');
	}
}
?>