<?php
if ($mod == "") {
    header('location:../../404.php');
} else {
    ?>
    <!-- 
    *******************************************************
            Include Header Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/header.php"; ?>


    <!-- 
    *******************************************************
            Main Content Template
    ******************************************************* 
    -->
    <?php
    $title = $val->validasi($_GET['idc'], 'xss');
    $tabledcat = new PoTable('category');
    $currentDcat = $tabledcat->findByAnd(seotitle, $title, active, 'Y');
    $currentDcat = $currentDcat->current();
    $iddcat = $currentDcat->id_category;
    ?>
    <?php if ($currentDcat != "0") { ?>
        <header class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1><?= $currentDcat->title; ?></h1>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <ul id="navTrail">
                            <li><a href="<?= $website_url; ?>">Beranda</a></li>                            
                            <li id="navTrailLast"><strong><?= $currentDcat->title; ?></strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <section id="content">
            <section class="pt30 pb30"> 
                <div class="container clearfix">
                    <div class="row">

                        <div class="portfolio-items  isotopeWrapper clearfix imgHover neko-hover-1" id="3">

                            <?php
                            $p = new Paging;
                            $batas = 5;
                            $posisi = $p->cariPosisi($batas);
                            $tabledcpost = new PoTable('post');
                            $dcposts = $tabledcpost->findAllLimitByAnd(id_post, id_category, active, "$iddcat", "Y", "DESC", "$posisi,$batas");
                            foreach ($dcposts as $dcpost) {
                                $tabledccom = new PoTable('comment');
                                $totaldccom = $tabledccom->numRowByAnd(id_post, $dcpost->id_post, active, 'Y');
                                $tableuser = new PoTable('users');
                                $currentUser = $tableuser->findBy(id_user, $dcpost->editor);
                                $currentUser = $currentUser->current();
                                ?>
                                <article class="col-sm-4 isotopeItem women">
                                    <div class="">
                                        <figure>
                                            <a href="<?php echo "$website_url/detailpost/$dcpost->seotitle"; ?>" class="image">
                                                <img src="<?= $website_url; ?>/po-content/po-upload/medium/medium_<?= $dcpost->picture; ?>" class="img-responsive" />
                                                <span class="hover-zoom"></span>
                                            </a>                                               

                                            <figcaption>
                                                <ul class="pinInfo">
                                                    <li><?= cuthighlight('title', $dcpost->title, '30'); ?></li>                                                    
                                                </ul>
                                                <div class="iconLinks">                                                
                                                    <a href="<?= $website_url; ?>/po-content/po-upload/medium/medium_<?= $dcpost->picture; ?>" class="image-link" title="Zoom" ><i class="icon-search"></i></a>
                                                </div>
                                            </figcaption>
                                        </figure>

                                        <section class="boxContent text-center">
                                            <h3><a href="<?php echo "$website_url/detailpost/$dcpost->seotitle"; ?>"><?= cuthighlight('title', $dcpost->title, '30'); ?></a></h3>  
                                            <p><?= cuthighlight('post', $dcpost->content, '100'); ?>...</p>
                                        </section>

                                    </div>
                                </article>                                                           
                            <?php } ?>


                            <?php
                            $p = new Paging;
                            $batas = 6;
                            $posisi = $p->cariPosisi($batas);
                            $tablegal = new PoTable('gallery');
                            $gallerys = $tablegal->findAllLimit(id_gallery, "DESC", "$posisi,$batas");
                            foreach ($gallerys as $gallery) {
                                $idalb = $gallery->id_album;
                                $tablecalb = new PoTable('album');
                                $currentCalb = $tablecalb->findBy(id_album, $idalb);
                                $currentCalb = $currentCalb->current();
                                if ($currentCalb->active == 'Y') {
                                    ?>


                                    <?php
                                }
                            }
                            ?>


                        </div>
                    </div>
                    <ul class="pagination">
                        <?php
                        $getpage = $val->validasi($_GET['page'], 'sql');
                        $jmldata = $tabledcpost->numRowByAnd(id_category, "$iddcat", active, "Y");
                        $jmlhalaman = $p->jumlahHalaman($jmldata, $batas);
                        $linkHalaman = $p->navHalaman($getpage, $jmlhalaman, $website_url, "category", $currentDcat->seotitle, "1");
                        echo "$linkHalaman";
                        ?>
                    </ul>
                </div>
            </section>
        </section>


        <?php
    } else {
        header('location:' . $website_url . '/404.php');
    }
    ?>


    <!-- 
    *******************************************************
            Include Footer Template
    ******************************************************* 
    -->
    <?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>