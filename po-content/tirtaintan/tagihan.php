<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>
<!-- 
*******************************************************
	Include Header Template
******************************************************* 
-->
<?php include_once "po-content/$folder/header.php"; ?>


<!-- 
*******************************************************
	Main Content Template
******************************************************* 
-->
    <section id="page">
        <header class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Info Tagihan Air</h1>
						<p>Catatan pembayaran tagihan rekening air</p>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <ul id="navTrail">
                            <li><a href="<?= $website_url; ?>">Beranda</a></li>                            
                            <li id="navTrailLast"><strong>Info Tagihan Air</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <section id="registrasi" class="color0">   
            <div class="mb30"></div>
            <div class="container mb30">

                <div class="row">
                    <div class="col-sm-12">
						<div class="form-group">
							<input id="pel_no" type="text" name="pel_no" class="form-control" maxlength="6" placeholder="6 Digit Nomer Sambungan" />
						</div>
						<div class="form-group">
							<button class="btn btn-secondary" onclick="cariTagihan()">Periksa</button>
							<button class="btn btn-secondary" onclick="cariFoto()">Foto Meter</button>
						</div>
					</div>
                </div>

				<div class="row">
					<div class="col-md-12"><h4 id="pel_nama"></h4></div>
					<div class="col-md-12"><p id="pel_alamat"></p></div>
					<div class="col-md-12"><p id="cabang"></p></div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="dataTable_wrapper hidden">
							<table class="table table-striped table-bordered table-hover" id="dataTables-tagihan"></table>
						</div>
					</div>
				</div>
				
            </div>

        </section>
    </section>
		
<!-- 
*******************************************************
	Include Footer Template
******************************************************* 
-->
<?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>

<!-- DataTable -->
<script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/dataTables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= $website_url; ?>/po-content/<?= $folder; ?>/assets/js-plugin/dataTables/dataTables.bootstrap.js"></script>

<script>
<?php
    if(isset($_GET['nosl'])){
?>
    	function setTagihan(param){
    		var dataFeed    = {pel_no: param};
    		localStorage.setItem('tirtaintan', JSON.stringify(dataFeed));
    		document.location.href  = '/info-tagihan';
    	}
    	
        var param = '<?php echo $_GET['nosl']; ?>';
        setTagihan(param);
<?php
    }
?>
</script>

<script>
	function cariTagihan(){
		var param       = jQuery('#pel_no').val();
		var dataFeed    = {pel_no: param};
		localStorage.setItem('tirtaintan', JSON.stringify(dataFeed));
		document.location.href  = '/info-tagihan';
	}
	function cariFoto(){
		var param       = jQuery('#pel_no').val();
		if(param.length>0){
			var dataFeed    = {pel_no: param};
			localStorage.setItem('tirtaintan', JSON.stringify(dataFeed));
		}
		else{
			var dataTemp    = JSON.parse(localStorage.tirtaintan);
			param			= dataTemp.pel_no;
		}
		document.location.href  = '/foto-cater/' + param;
	}
	jQuery(document).ready(function() {
		var dataTemp    = {};
		var dataProc    = {};
		var dataFeed    = {};
		var bulan		= {};
		if(typeof(localStorage.tirtaintan)=='string'){
			dataTemp        = JSON.parse(localStorage.tirtaintan);
			jQuery('#dataTables-tagihan').DataTable({
				responsive: true,
				searching: false,
				processing: true,
				serverSide: true,
				ajax: {
					url: 'https://pdam.tirtaintan.co.id/bot/view_rinci.php?data=' + dataTemp.pel_no,
					type: 'POST',
					data: {filter: [{name: 'pel_no', value: dataTemp.pel_no}]},
					dataFilter: function(data){
						dataProc = jQuery.parseJSON(data);
						if(dataProc.data.length>0){
							jQuery('.dataTable_wrapper').removeClass('hidden');
							dataFeed = dataProc.data[0];
							jQuery.each(dataProc.data, function(index, value){
								bulan[index] = value.rek_bln;
							});
							dataFeed.bulan = bulan;
							jQuery('#pel_no').attr('placeholder', 'Nomer Sambungan ' + dataFeed.pel_no);
							jQuery('#pel_nama').html(dataFeed.pel_nama);
							jQuery('#pel_alamat').html(dataFeed.pel_alamat);
							jQuery('#cabang').html('Area Pelayanan ' + dataFeed.kp_ket);
							localStorage.setItem('tirtaintan', JSON.stringify(dataFeed));
						}
						else{
							if(typeof(dataTemp.pel_no)=='string'){
								jQuery('#pel_no').attr('placeholder', 'Nomer Sambungan ' + dataTemp.pel_no);
							}
							jQuery('#pel_nama').html('Data pelanggan tidak ditemukan');
							jQuery('#pel_alamat').html('Periksa kembali nomer sambungan yang anda cari');
						}
						return JSON.stringify(dataProc);
					}
				},
				columns: [
					{ title: "Bulan", data: "rek_bln", className: "text-right" },
					{ title: "Stanlalu", data: "rek_stanlalu", className: "hidden-xs text-right" },
					{ title: "Stankini", data: "rek_stankini", className: "hidden-xs text-right" },
					{ title: "Pemakaian", data: "rek_pakai", className: "hidden-xs text-right" },
					{ title: "Biaya Air", data: "rek_uangair", className: "hidden-xs text-right" },
					{ title: "Biaya Beban", data: "rek_beban", className: "hidden-xs text-right" },
					{ title: "Denda", data: "rek_denda", className: "hidden-xs text-right" },
					{ title: "Total", data: "rek_ditagih", className: "text-right" },
					{ title: "Dibayar", data: "byr_tgl", className: "hidden-xs text-right" },
					{ title: "Loket", data: "byr_loket", className: "col-md-2" }
				]
			});
		}
	});
</script>
