<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>
<!-- 
*******************************************************
	Include Header Template
******************************************************* 
-->
<?php include_once "po-content/$folder/header.php"; ?>


<!-- 
*******************************************************
	Main Content Template
******************************************************* 
-->
    <section id="header">
        <header class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1>Status Registrasi</h1>
						<p>Halaman untuk calon pelanggan baru, melihat progres pemasangan instalasi air minum</p>
                    </div>
                    <div class="col-sm-6 hidden-xs">
                        <ul id="navTrail">
                            <li><a href="<?= $website_url; ?>">Beranda</a></li>                            
                            <li id="navTrailLast"><strong>Status Registrasi</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
	</section>
	
	<section id="content">
		<div class="mb30"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<input id="client-id" type="text" class="form-control" maxlength="6" placeholder="Masukan 6 Digit Nomer Registrasi" />
					</div>
				</div>
				<div class="col-sm-12">
						<div class="form-group">
							<button class="btn btn-secondary">Periksa</button>
						</div>
				</div>
			</div>
		</div>	
		<div class="container">
			<div class="row">
				<div class="col-md-12"><h4 id="client-nama"></h4></div>
				<div class="col-md-12"><p id="client-alamat"></p></div>
				<div class="col-md-12"><p id="client-nopel"></p></div>
			</div>
		</div>	
		<div class="container">
			<div class="row">
				<table id="tabel-info" class="table hidden">
					<thead>
						<tr>
							<th>Keterangan</th>
							<th>Pelaksanaan</th>
						</tr>
					</thead>
					<tbody id="body-info"></tbody>
				</table>
			</div>
		</div>
	</section>

<!-- 
*******************************************************
	Include Footer Template
******************************************************* 
-->
<?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>

	<script>
		$(document).ready(function() {
			$('button').click(function(){
				var clientId	= $('#client-id').val();
				var targetUrl 	= "https://pdam.tirtaintan.co.id/bot/view_register.php?data=" + clientId;
				var inHTML		= "";
				var dataProc	= {};
				if (clientId.length==6){
					$.getJSON(targetUrl, function(data){
						if(data.data.length==1){
							dataProc = data.data[0];
							$('#client-nama').html(dataProc.client_nama);
							$('#client-alamat').html(dataProc.client_alamat);
							$('#client-nopel').html('<b>Nomer sambungan ' + dataProc.pel_no + '</b>');
							inHTML = '<tr><td>Registrasi</td><td>' + dataProc.tgl_registrasi + '</td></tr>' +
								'<tr><td>Pembayaran</td><td>' + dataProc.tgl_bayar + '</td></tr>' +
								'<tr><td>Pemasangan</td><td>' + dataProc.tgl_pasang + '</td></tr>' +
								'<tr><td>Pengaktifan</td><td>' + dataProc.tgl_aktif + '</td></tr>';
							$('#tabel-info').removeClass('hidden');
							$('#body-info').html(inHTML);
						}
						else{
							alert('Data pemohon tidak ditemukan');
						}
					});
				}
				else{
					alert('Masukan 6 digit nomer register');
				}
			});
		});
	</script>
